#include<iostream>
#include<vector>
#include<algorithm>
#include<bitset>

using namespace std;

//const int N = 2e5;
//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		int n, k;
//		cin >> n >> k;
//		bitset<N>s; // 全1
//		s.set();
//		int sum = n; // 全1
//		int subi = 0;
//		vector<int>starti; // 存储过程起点
//		int cnt = 0;
//		do {
//			starti.push_back(subi + 1);
//			++cnt;
//			for (int i = 0; i < k; ++i, subi = (subi + 1) % n)
//			{
//				sum += (s[subi] ? -1 : 1);
//				s[subi] = !s[subi];
//			}
//		} while (sum != 0 && sum != n && cnt <= n * 2);
//		if (sum == 0)
//		{
//			cout << cnt << endl;
//			for (auto i : starti) cout << i << " ";
//			cout << endl;
//		}
//		else
//		{
//			cout << -1 << endl;
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		int N, M, k;
//		cin >> N >> M >> k;
//		int n = N, m = M;
//		while (true)
//		{
//			long long sum = 0;
//			int subn = 0, subm = 0;
//			while (m && sum + 2 <= k)
//			{
//				--m;
//				++subm; // 预删除
//				sum += 2;
//			}
//
//			while (n && sum + 1 <= k)
//			{
//				--n;
//				++subn;
//				++sum;
//			}
//
//			if (sum < k)
//			{
//				n += subn;
//				m += subm;
//				break;
//			}
//		}
//
//		// 剩余男生多,尽量转为女生
//		while (n >= 2 && m < M)
//		{
//			n -= 2;
//			++m;
//		}
//		cout << n + m << endl;
//	}
//	return 0;
//}

//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		int i, j, k;
//		cin >> i >> j >> k;
//		long long sum = 0;
//		while (i <= j)
//		{
//			sum += (i % 2);
//			++i;
//		}
//		cout << sum % 2 << endl;
//	}
//	return 0;
//}

//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		int n;
//		cin >> n;
//		vector<int>srcs(n);
//		for (int i = 0; i < n; ++i) cin >> srcs[i];
//		sort(srcs.begin(), srcs.end());
//		long long sum = 0;
//		if (srcs.front() == srcs.back())
//		{
//			sum = srcs.front() * (n - 2);
//		}
//		else
//		{
//			for (int i = 1; i < n - 1; ++i) sum += srcs[i];
//			++sum;
//		}
//		cout << sum << endl;
//
//	}
//	return 0;
//}