#include<iostream>
#include<cmath>

using namespace std;

int pos(int n)
{
	return n * n;
}

void test()
{
	int x, y, z;

	if (pos(x) + pos(y) > pos(z) && pos(x) + pos(z) > pos(y) && pos(y) + pos(z) > pos(x)) // 锐角
	{
		printf("Acute triangle");//锐角三角形
		if ((x == y && x != z) || (x == z && x != y) || (y == z && y != x))
			printf("Isosceles triangle");//等腰锐角三角形
		if (x == y && y == z)
			printf("Equilateral triangle");//等边锐角三角形
	}
}

int main()
{
	int x, y, z;
	scanf("%d %d %d", &x, &y, &z);
				// 可以在上面提前判断一下，方便后面比较
	if (x > y) swap(x, y); // swap是c++里自带的函数， 使用C语言没有，可以自己写一个交换
				// 下面 x 一定 <= y
	if (y > z) swap(y, z);
				// 下面 x <=y <= z

	if (x == z) printf("等边\n");
	else		// x != z
		if (x == y || y == z) printf("等腰\n");
		else printf("普通\n");

	return 0;
}