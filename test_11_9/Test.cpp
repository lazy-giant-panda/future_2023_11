#include<vector>
#include<unordered_map>

using namespace std;

class Solution {
public:
    int totalFruit(vector<int>& fruits) {
        unordered_map<int, int>hash; // 种类 个数
        int res = 0;
        for(int left = 0, right = 0; right < fruits.size(); ++right)
        {
            // 水果加入map映射， 如果水果种类大于2就从左往右删除，直到一个为0
            ++hash[fruits[right]];
            while (hash.size() > 2)
            {
                if (--hash[fruits[left]] == 0) hash.erase(fruits[left]);
                ++left;
            }
            res = max(res, right + 1 - left);
        }
        return res;
    }
};